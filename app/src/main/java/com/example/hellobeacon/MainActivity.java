package com.example.hellobeacon;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity";


    //Delimiter used in file
    public static final String COMMA_DELIMITER = ",";

    //new line
    private static final String NEW_LINE_SEPARATOR = "\n";

    //file header
    private static final String FILE_HEADER = "UUID,MAJOR,MINOR";

    private EditText edt_interval;
    private List<Beacon> scannedBeacons = new ArrayList<>();
    private TableLayout tableLayout;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLayout();


        //TODO: get intent
        intent = getIntent();
    }

    private void initLayout() {
        edt_interval = findViewById(R.id.interval);
        tableLayout = findViewById(R.id.tableLayout);

    }

    @Override
    protected void onResume() {
        //TODO: get intent and add beacons to List
        if(intent.hasExtra(ServiceImpl.SCANNED_BEACONS)){
            List<Beacon> beacons = (List<Beacon>) intent.getSerializableExtra(ServiceImpl.SCANNED_BEACONS);
            scannedBeacons = beacons;
            showBeaconsInLinearLayout();
        }
        super.onResume();
    }

    private void showBeaconsInLinearLayout() {
        //TODO: implement this
        Log.d(TAG, scannedBeacons.size()+"");
        if(scannedBeacons.size()>0){
            int rowCount = 4;
            for(int i=0;i<scannedBeacons.size();i++){
                TableRow row= new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                row.setWeightSum(6);
                row.setPadding(5,5,5,5);

                TableRow.LayoutParams param = new TableRow.LayoutParams(
                        0,
                        TableRow.LayoutParams.WRAP_CONTENT,
                        1.0f
                );

                TextView uuid_left = new TextView(this);
                uuid_left.setText(scannedBeacons.get(i).getUUID());
                uuid_left.setLayoutParams(param);
                uuid_left.setPadding(0,0,10,0);
                row.addView(uuid_left);

                TextView major_left = new TextView(this);
                major_left.setText(String.valueOf(scannedBeacons.get(i).getMajor()));
                major_left.setLayoutParams(param);
                major_left.setPadding(15,0,0,0);
                row.addView(major_left);

                TextView minor_left = new TextView(this);
                minor_left.setText(String.valueOf(scannedBeacons.get(i).getMinor()));
                minor_left.setLayoutParams(param);
                minor_left.setPadding(15,0,0,0);
                row.addView(minor_left);

                if(i+1<scannedBeacons.size()){
                    i++;
                    TextView uuid_right = new TextView(this);
                    uuid_right.setText(scannedBeacons.get(i).getUUID());
                    uuid_right.setLayoutParams(param);
                    uuid_right.setPadding(0,0,10,0);
                    row.addView(uuid_right);

                    TextView major_right = new TextView(this);
                    major_right.setText(String.valueOf(scannedBeacons.get(i).getMajor()));
                    major_right.setLayoutParams(param);
                    major_right.setPadding(15,0,0,0);
                    row.addView(major_right);

                    TextView minor_right = new TextView(this);
                    minor_right.setText(String.valueOf(scannedBeacons.get(i).getMinor()));
                    minor_right.setLayoutParams(param);
                    minor_right.setPadding(15,0,0,0);
                    row.addView(minor_right);
                }

                tableLayout.addView(row,rowCount++);

                TextView horizontal_line = new TextView(this);
                horizontal_line.setLayoutParams( new TableRow.LayoutParams(
                        TableRow.LayoutParams.MATCH_PARENT,
                        2
                ));
                horizontal_line.setBackgroundResource(R.color.dark_grey);
                tableLayout.addView(horizontal_line,rowCount++);

            }
        }

    }

    //Do not change this!
    protected void writeBeaconSimulationFile(){

        //Create new beacon objects
        Beacon beacon1 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,1);
        Beacon beacon2 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,2);
        Beacon beacon3 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,3);
        Beacon beacon4 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,4);
        Beacon beacon5 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,5);
        Beacon beacon6 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,8);
        Beacon beacon7 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,9);
        Beacon beacon8 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",4,10);
        Beacon beacon9 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,10);
        Beacon beacon10 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,9);
        Beacon beacon11 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,8);
        Beacon beacon12 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,5);
        Beacon beacon13 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,4);
        Beacon beacon14 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,3);
        Beacon beacon15 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,2);
        Beacon beacon16 = new Beacon("EBBD7150-D911-11E4-8830-0800200C9A66",3,1);

        //Create a new list of beacons objects
        ArrayList<Beacon> beacons = new ArrayList<Beacon>();
        beacons.add(beacon1);
        beacons.add(beacon2);
        beacons.add(beacon3);
        beacons.add(beacon4);
        beacons.add(beacon5);
        beacons.add(beacon6);
        beacons.add(beacon7);
        beacons.add(beacon8);
        beacons.add(beacon9);
        beacons.add(beacon10);
        beacons.add(beacon11);
        beacons.add(beacon12);
        beacons.add(beacon13);
        beacons.add(beacon14);
        beacons.add(beacon15);
        beacons.add(beacon16);
        beacons.add(beacon15);
        beacons.add(beacon14);
        beacons.add(beacon3);
        beacons.add(beacon2);
        beacons.add(beacon1);


        try{
            FileOutputStream testFile = openFileOutput(ServiceImpl.FILE_NAME, Context.MODE_APPEND);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(testFile);
            outputStreamWriter.append(FILE_HEADER.toString());
            outputStreamWriter.append(NEW_LINE_SEPARATOR);

            for (Beacon beacon : beacons) {
                outputStreamWriter.append(String.valueOf(beacon.getUUID()));
                outputStreamWriter.append(COMMA_DELIMITER);
                outputStreamWriter.append(String.valueOf(beacon.getMajor()));
                outputStreamWriter.append(COMMA_DELIMITER);
                outputStreamWriter.append(String.valueOf(beacon.getMinor()));
                outputStreamWriter.append(NEW_LINE_SEPARATOR);
            }

            outputStreamWriter.close();
        }
        catch (IOException ex){
            Log.d("Message", ex.getMessage());
        }
    }

    //TODO: button starts the service
    public void startServiceByButtonClick(View v) {
        //TODO: Get user input

        String txt_interval = edt_interval.getText().toString().trim();
        if(TextUtils.isEmpty(txt_interval)){
            edt_interval.setError("This is required");
            edt_interval.requestFocus();
            return;
        }
        int interval = Integer.parseInt(txt_interval);
        Log.d(TAG, "interval is: "+ interval);
        //Do not change this!
        File dir = getFilesDir();
        File file = new File(dir, ServiceImpl.FILE_NAME);
        boolean deleted = file.delete();
        Log.d(TAG, "old file deleted "+ deleted);
        Log.d(TAG, "Beacons written to file "+ file.getAbsolutePath());

        //this method writes the file containing simulated beacon data
        writeBeaconSimulationFile();

        //TODO: Service is started via intent
        startService(interval);

    }

    private void startService(int interval) {
        Intent intent = new Intent(this, ServiceImpl.class);
        intent.putExtra(ServiceImpl.INTERVAL, interval);
        startService(intent);
        Log.d(TAG, "Service started");
    }

    //TODO: stop service
    public void stopServiceByButtonClick(View v) {
        //implement this
        stopService(new Intent(this, ServiceImpl.class));
    }

}