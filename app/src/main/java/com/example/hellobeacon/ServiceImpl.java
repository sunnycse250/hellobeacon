package com.example.hellobeacon;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceImpl extends IntentService {

    public static final String FILE_NAME = "Beacons.txt";
    private static final String CHANNEL_ID = "com.example.hellobeacon.ServiceImpl";
    private String TAG = "ServiceImpl";
    private long seconds;
    private  java.io.File file;
    private  BufferedReader reader;
    private int lineNumber;
    private List<Beacon> scannedBeacons = new ArrayList<>();
    public static final String INTERVAL = "INTERVAL";
    public static final String SCANNED_BEACONS = "SCANNED_BEACONS";
    //TODO: include needed fields

    public ServiceImpl() {
        super("ServiceImpl");
    }

    protected void onHandleIntent(Intent intent) {

        createNotificationChannel();
        setupInputReader();

        //TODO: get the seconds from intent

        //how long the service should sleep, in milliseconds
        seconds = intent.getIntExtra(INTERVAL, 0);
        long millis = seconds * 1000;
        while (true) {
            try {
                Beacon beacon = scanBeacon();

                if(beacon != null){
                    //TODO: add beacons to the List of scanned beacons
                    scannedBeacons.add(beacon);

                    //TODO: notification
                    NotificationCompat.Builder builder = createNotification(beacon);

                    //TODO: intent to AddBeaconsActivity
                    Intent intentAddBeacons = new Intent (this, AddBeaconsActivity.class);
                    intentAddBeacons.putExtra(SCANNED_BEACONS, (Serializable) scannedBeacons);

                    //build intent to switch to activity on click
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

                    //adds the back stack for the Intent (not the intent itself)
                    stackBuilder.addParentStack(AddBeaconsActivity.class);

                    //adds the intent that starts and puts the activity to the top of the stack
                    //TODO: uncomment this and insert the above created intent as input
                    stackBuilder.addNextIntent(intentAddBeacons);

                    //PendingIntent waits for an event
                    PendingIntent scanResultPendingIntent = stackBuilder
                            .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(scanResultPendingIntent);

                    //TODO: create notification manager
                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify((int) (Math.random()*5), builder.build());


                }

                //TODO: put the service to sleep
               Thread.sleep(millis);

            } catch (InterruptedException iEx) {
                Log.d("Message", iEx.getMessage());
            }
        }
    }

    private NotificationCompat.Builder createNotification(Beacon beacon) {
        return new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("New Beacon found")
                .setContentText(beacon.toString())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "my channel";
            String description = "my channel description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void setupInputReader() {

        //TODO: read the file "Beacon.txt"
        //read the header in advance to exclude it from the output


        try {
            File dir = getFilesDir();
            file = new File(dir, ServiceImpl.FILE_NAME);
            reader = new BufferedReader(new FileReader(String.valueOf(file)));
            String text = reader.readLine();
            lineNumber++;
            Log.d(TAG, "Header read"+ text);
        } catch (IOException e){
            Log.d(TAG, e.getMessage());
        }

    }

    private Beacon scanBeacon() {
        try{
            //TODO: Read a line and split one row into the beacon components uuid, major and minor
            //create a new beacon and return it
            lineNumber++;
            String currentLine = reader.readLine();
            Log.d(TAG, "Current beacon read lineNumber: "+ lineNumber
                    + " currentLine " + currentLine);
            if(currentLine==null){
                Log.d(TAG, "No more beacons in the file");
                return null;
            }
            String[] currentBeacons = currentLine.split(MainActivity.COMMA_DELIMITER);
            return new Beacon(currentBeacons[0], Integer.parseInt(currentBeacons[1]),
                    Integer.parseInt(currentBeacons[2]));
        }catch (IOException e){
            Log.d("Message", e.getMessage());
            return null;
        }
    }

    public void onDestroy() {
        //TODO: implement this
        Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
        try{
            reader.close();
            Log.d(TAG, "Reader closed successfully");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Could not  close reader");
        }
    }
}
