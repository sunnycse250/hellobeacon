package com.example.hellobeacon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddBeaconsActivity extends AppCompatActivity {

    private TableLayout tableLayout;
    private List<Beacon> scannedBeacons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_beacons);
        initLayout();
        getData();
    }

    private void initLayout(){
        tableLayout = findViewById(R.id.tableLayout);
    }

    private void getData(){
        Intent intent = getIntent();
        if(intent.hasExtra(ServiceImpl.SCANNED_BEACONS)){
            scannedBeacons = (List<Beacon>) intent.getSerializableExtra(ServiceImpl.SCANNED_BEACONS);
            createTableRows();
        }
    }



    // DisplayHelper:
    private Float scale;
    public int dpToPixel(int dp, Context context) {
        if (scale == null)
            scale = context.getResources().getDisplayMetrics().density;
        return (int) ((float) dp * scale);
    }

    private void createTableRows() {
        for(int i=0;i<scannedBeacons.size();i++){
            TableRow row= new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            row.setWeightSum(3);


            TableRow.LayoutParams param = new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.0f
            );
            param.rightMargin = dpToPixel(15, this);

            TextView uuid = new TextView(this);
            uuid.setText(scannedBeacons.get(i).getUUID());
            row.addView(uuid);
            uuid.setLayoutParams(param);

            TextView major = new TextView(this);
            major.setText(String.valueOf(scannedBeacons.get(i).getMajor()));
            row.addView(major);
            major.setLayoutParams(param);

            TextView minor = new TextView(this);
            minor.setText(String.valueOf(scannedBeacons.get(i).getMinor()));
            row.addView(minor);
            minor.setLayoutParams(param);
            minor.setPadding(15,0,0,0);

            row.setBackground(getResources().getDrawable(R.drawable.add_beacon_table_item_bg));
            tableLayout.addView(row,i+1);
        }
    }

    public void addScannedBeacons(View view) {
        Intent intent = new Intent (this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ServiceImpl.SCANNED_BEACONS, (Serializable) scannedBeacons);
        startActivity(intent);
    }
}